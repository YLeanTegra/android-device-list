#!/usr/bin/env node

"use strict";

const config = require("./config.json"),
  fs = require("fs"),
  path = require('path');

function getDataFromName(file) {
  return file.split("-").map(file => {
    return file.replace(/\.json$/, "");
  });
}

function readFile(file) {
  return new Promise((res, rej) => {
    fs.readFile(file, (err, data) => {
      if (err) return rej(err);

      res(JSON.parse(data));
    });
  });
}

const DEFAULT_INTERVAL = 3600000;

let data = readDir(config.buildDir, parseInt(config.interval) || DEFAULT_INTERVAL);

function readDir(dir, interval) {
  return new Promise((res, rej) => {
    fs.readdir(dir, async (err, files) => {
      let fileNames = {};

      if (!err) {
        fileNames = files.reduce((acc, file) => {
          const [type, date] = getDataFromName(file);

          acc[type] =
            !acc[type] || parseInt(getDataFromName(acc[type])[1]) < date
              ? file
              : acc[type];

          return acc;
        }, {});
      }

      const fileData = {};

      fileData.devices = await readFile(fileNames.devices || path.join(__dirname, "devices.json"));
      fileData.brands = await readFile(fileNames.brands || path.join(__dirname, "brands.json"));

      setTimeout(() => (data = readDir(dir, interval)), interval);

      res(fileData);
    });
  });
}

async function deviceList() {
  return (await data).devices;
}

async function brandList() {
  return (await data).brands;
}

async function getDevices(find, field, caseInsensitive, contains) {
  return (await data).devices.filter(device => {
    return (
      device[field] === find ||
      (contains && device[field].indexOf(find) !== -1) ||
      (caseInsensitive && device[field].toLowerCase() === find.toLowerCase()) ||
      (caseInsensitive &&
        contains &&
        device[field].toLowerCase().indexOf(find.toLowerCase()) !== -1)
    );
  });
}

function getDevicesByBrand(brand, options) {
  if (typeof brand !== "string") {
    throw new TypeError("`brand` parameter must be a string");
  }
  options = options || {};
  let caseInsensitive = !!options.caseInsensitive;
  let contains = !!options.contains;

  return getDevices(brand, "brand", caseInsensitive, contains);
}

function getDevicesByName(name, options) {
  if (typeof name !== "string") {
    throw new TypeError("`name` parameter must be a string");
  }
  options = options || {};
  let caseInsensitive = !!options.caseInsensitive;
  let contains = !!options.contains;

  return getDevices(name, "name", caseInsensitive, contains);
}

function getDevicesByDeviceId(deviceId, options) {
  if (typeof deviceId !== "string") {
    throw new TypeError("`deviceId` parameter must be a string");
  }
  options = options || {};
  let caseInsensitive = !!options.caseInsensitive;
  let contains = !!options.contains;

  return getDevices(deviceId, "device", caseInsensitive, contains);
}

function getDevicesByModel(model, options) {
  if (typeof model !== "string") {
    throw new TypeError("`model` parameter must be a string");
  }
  options = options || {};
  let caseInsensitive = !!options.caseInsensitive;
  let contains = !!options.contains;

  return getDevices(model, "model", caseInsensitive, contains);
}

module.exports = {
  deviceList,
  brandList,
  getDevicesByBrand,
  getDevicesByName,
  getDevicesByDeviceId,
  getDevicesByModel
};
